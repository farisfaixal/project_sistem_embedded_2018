/*
  LiquidCrystal Temperature Logger.
Developed using Arduino Uno R3

Pins configuration for standard HD44780 LCD
This sketch is for 16 character x 2 row LCD

* LCD Pin 1 to Ground
* LCD Pin 2 to +5
* LCD Pin 3 to Center Leg Pot 10k
* LCD Pin 4 to digital pin 12
* LCD pin 5 to Ground
* LCD pin 6 to digital pin 10
* LCD pin 11 to digital pin 5
* LCD pin 12 to digital pin 4
* LCD pin 13 to digital pin 3
* LCD pin 14 to digital pin 2
* LCD pin 15 +5 (LED Backlight +) *optional
* LCD pin 16 to Ground (LED Backlight -) *optional

Pin configuration for LM34DZ http://www.ti.com/lit/ds/symlink/lm34.pdf

* Vs   to +5
* Vout to Analog pin A0
* GND  to Ground

Sketch created by David A. Smith 6-22-2012

Modified from
http://reversemidastouch.blogspot.com/2010/03/arduino-lm34-temperature-sensor.html
"eeprom_write" from File > Examples > EEPROM > eeprom_write

*/

// include the library code:
#include <EEPROM.h>
#include <LiquidCrystal.h>
#include <SHT1x.h>

int addr = 0;                   // the current address in the EEPROM we're going to write to
//int sensor_pin = 0;             // the analog pin for LM34 temp sensor
//int sensor_reading = 0.0;       // variable to store the value coming from the sensor
//float vref = 4.85;              // variable to store the voltage reference used
                                // using a separate thermometer tweak vref value to fine tune the displayed temperature
// Specify data and clock connections and instantiate SHT1x object
#define dataPin  10
#define clockPin 11
SHT1x sht1x(dataPin, clockPin);

int fahrenheit = 0;             // variable to store the fahrenheit temperature
int centigrade = 0;             // variable to store the centigrade temperature
int acquisition_timer = 5000;   // variable to control the time between updates (in ms)
                                // 300000 = 5 min intervals
int loop_counter = 0;
int bytemax = 1024;             // adjust this value for your eeprom memory limit
                                // there are 1024 bytes in the ATmega328 EEPROM
                                // 512 bytes on the ATmega168 and ATmega8
                                // 4 KB (4096 bytes) on the ATmega1280 and ATmega2560

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(12, 11, 5, 4, 3, 2);


void setup() {
  // set up the LCD's number of columns and rows: 
  lcd.begin(16,2);
  
}

void loop() {
  
  float fahrenheit = sht1x.readTemperatureF();   // calculates the actual fahrenheit temperature
  float centigrade = sht1x.readTemperatureC();;                  //conversion to degrees C

// set the cursor to (0,0):
   lcd.setCursor(0,0);
   lcd.print(centigrade);
   lcd.print((char)223);   // degree symbol
   lcd.print("C ");
   lcd.print(fahrenheit);
   lcd.print((char)223);   // degree symbol
   lcd.print("F");

//Print readings counter on the second row
   lcd.setCursor(0, 1);
   lcd.print("Readings: ");
   
// if the eeprom is not full 
   if (loop_counter <= bytemax) {
   lcd.print(loop_counter);
   
// divide by 4 because analog inputs range from 0 to 1023 and
// each byte of the EEPROM can only hold a value from 0 to 255.
   int val = centigrade;

// write the value to the appropriate byte of the EEPROM.
// these values will remain there when the board is turned off.
   EEPROM.write(addr, val);

// advance to the next address.
// there are 1024 bytes in the ATmega328 EEPROM
// 512 bytes on the ATmega168 and ATmega8, 4 KB (4096 bytes) on the ATmega1280 and ATmega2560
  addr = addr + 1;
  delay (acquisition_timer);
  }
  
    else {
      lcd.print("Full");
      delay(acquisition_timer);          // 300000 = 5 min intervals
    }
    loop_counter++;

// turn off automatic scrolling
   lcd.noAutoscroll();
  
// clear screen for the next loop:
   lcd.clear();  
}

