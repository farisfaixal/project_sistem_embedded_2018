int led = 10;
int kecepatan = 100;
//kaki arduino mega memiliki 53 kaki digital
//kaki analog dari a0 - a15 --> sensor analog 

void setup() {
  // put your setup code here, to run once:
  pinMode(led, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  digitalWrite(led, 1); //led diberikan tegangan 5 volt (logika 1)
  delay(kecepatan); //delay selama 1000uS = 1 detik
  digitalWrite(led, 0); //led diberikan tegangan 0 volt (lodika 0)
  delay(kecepatan);
}
