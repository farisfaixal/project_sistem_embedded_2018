-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2019 at 02:12 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tugasembed`
--

-- --------------------------------------------------------

--
-- Table structure for table `keruh`
--

CREATE TABLE `keruh` (
  `id_keruh` int(11) NOT NULL,
  `aran` varchar(100) NOT NULL,
  `data` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `keruh`
--

INSERT INTO `keruh` (`id_keruh`, `aran`, `data`) VALUES
(1, 'Keruh', '2838.80'),
(2, 'Keruh', '2855.23'),
(3, 'Keruh', '2838.80'),
(4, 'Keruh', '2859.20'),
(5, 'Keruh', '2825.91'),
(6, 'Keruh', '2825.91'),
(7, 'Keruh', '2825.91'),
(8, 'Keruh', '2821.51'),
(9, 'Keruh', '2821.51'),
(10, 'Keruh', '2821.51'),
(11, 'Keruh', '2821.51'),
(12, 'Keruh', '2825.91'),
(13, 'Keruh', '2738.47'),
(14, 'Keruh', '2738.47'),
(15, 'Keruh', '2733.10'),
(16, 'Keruh', '2738.47'),
(17, 'Keruh', '2754.25'),
(18, 'Keruh', '2743.78'),
(19, 'Keruh', '2754.25'),
(20, 'Keruh', '2774.54'),
(21, 'Keruh', '2754.25'),
(22, 'Keruh', '2830.26'),
(23, 'Keruh', '2727.68'),
(24, 'Keruh', '695.10'),
(25, 'Keruh', '819.20'),
(26, 'Keruh', '864.85'),
(27, 'Keruh', '864.85'),
(28, 'Keruh', '2269.45'),
(29, 'Keruh', '879.96'),
(30, 'Keruh', '803.87'),
(31, 'Keruh', '803.87'),
(32, 'Keruh', '834.47'),
(33, 'Keruh', '757.58'),
(34, 'Keruh', '710.80'),
(35, 'Keruh', '2561.11'),
(36, 'Keruh', '2287.09'),
(37, 'Keruh', '2260.55'),
(38, 'Keruh', '2321.72'),
(39, 'Keruh', '2304.52'),
(40, 'Keruh', '1893.62'),
(41, 'Keruh', '-459.43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `keruh`
--
ALTER TABLE `keruh`
  ADD PRIMARY KEY (`id_keruh`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `keruh`
--
ALTER TABLE `keruh`
  MODIFY `id_keruh` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
