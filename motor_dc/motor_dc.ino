int motorPin = 3;
int input1 = 4;
int input2=5;
void setup() 
{ 
pinMode(motorPin, OUTPUT);
Serial.begin(9600);
while (! Serial);
Serial.println("Speed 0 to 255");
} 
void loop() 
{ 
if (Serial.available())
{
int speed = Serial.parseInt();
if (speed >= 0 && speed <= 255)
{
digitalWrite(input1, HIGH);
digitalWrite(input2, LOW);
analogWrite(motorPin, speed);
}
}
} 

